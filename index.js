
// (1)
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(array => array.map(function(object) {
    return object.title;
}))
.then(newArray => console.log(newArray))


// (2)
async function fetchData() {
	let item = await fetch("https://jsonplaceholder.typicode.com/todos/1");
	let json = await item.json();
	console.log(json);
	console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
};
fetchData();


// (3)
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List item",
		userId: 1
	})
})
.then(response => response.json())
.then(postedItem => console.log(postedItem));


// (4)
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the My To Do List with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// (5)
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete",
	})
})
.then(res => res.json())
.then(json => console.log(json));


// (6)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE",
})
